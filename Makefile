CC = gcc
CFLAGS = -g -Wall -Wshadow -Wno-unused-function `pkg-config --cflags gstreamer-1.0 gstreamer-app-1.0`
LDFLAGS = `(pkg-config --libs gstreamer-1.0 gstreamer-app-1.0)`

SRCS = $(wildcard *.c)

all:
	make clean
	make build
	./splitmuxsrc-test

debug:
	make clean
	make build
	env GST_DEBUG=3,splitmuxsrc:6,queue:6,splitmuxpartreader:6 ./splitmuxsrc-test

build:
	$(CC) $(CFLAGS) $(LDFLAGS) $(SRCS) -o splitmuxsrc-test

clean:
	$(RM) splitmuxsrc-test
