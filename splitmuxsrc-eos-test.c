#include <assert.h>
#include <gst/app/app.h>
#include <gst/gst.h>
#include <signal.h>
#include <string.h>

GstClockTime first_ts;
GstClockTime last_ts;
gdouble current_rate;

static void
seek_pipeline (GstElement * pipeline, gdouble rate,
    GstClockTime start, GstClockTime end)
{
  /* Pause the pipeline, seek to the desired range / rate, wait for PAUSED
   * again, then clear the tracking vars for start_ts / end_ts */
  gst_element_set_state (pipeline, GST_STATE_PAUSED);
  gst_element_get_state (pipeline, NULL, NULL, GST_CLOCK_TIME_NONE);

  /* specific end time not implemented: */
  g_assert (end == GST_CLOCK_TIME_NONE);

  gst_element_seek (pipeline, rate, GST_FORMAT_TIME,
      GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE,
      GST_SEEK_TYPE_SET, start, GST_SEEK_TYPE_END, 0);

  /* Wait for the pipeline to preroll again */
  gst_element_get_state (pipeline, NULL, NULL, GST_CLOCK_TIME_NONE);

  GST_LOG ("Seeked pipeline. Rate %f time range %" GST_TIME_FORMAT
      " to %" GST_TIME_FORMAT,
      rate, GST_TIME_ARGS (start), GST_TIME_ARGS (end));

  /* Clear tracking variables now that the seek is complete */
  first_ts = last_ts = GST_CLOCK_TIME_NONE;
  current_rate = rate;
};

static GstMessage *
run_pipeline (GstElement * pipeline)
{
  GstBus *bus = gst_element_get_bus (GST_ELEMENT (pipeline));
  GstMessage *msg;

  gst_element_set_state (pipeline, GST_STATE_PLAYING);
  gst_debug_bin_to_dot_file (GST_BIN (pipeline), GST_DEBUG_GRAPH_SHOW_VERBOSE,
      "splitmuxsrc-eos-test");
  msg = gst_bus_poll (bus, GST_MESSAGE_EOS | GST_MESSAGE_ERROR, -1);
  gst_element_set_state (pipeline, GST_STATE_NULL);

  gst_object_unref (bus);

  if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR)
    g_print ("Message error\n");
  if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_EOS)
    g_print ("Message EOS\n");

  return msg;
}

static GstFlowReturn
receive_sample (GstAppSink * appsink, gpointer user_data)
{
  GstSample *sample;
  GstSegment *seg;
  GstBuffer *buf;
  GstClockTime start;
  GstClockTime end;

  g_signal_emit_by_name (appsink, "pull-sample", &sample);
  assert (sample != NULL);

  seg = gst_sample_get_segment (sample);
  assert (seg != NULL);

  buf = gst_sample_get_buffer (sample);
  assert (buf != NULL);

  GST_LOG ("Got buffer %" GST_PTR_FORMAT, buf);

  start = GST_BUFFER_PTS (buf);
  end = start;

  if (GST_CLOCK_TIME_IS_VALID (start))
    start = gst_segment_to_stream_time (seg, GST_FORMAT_TIME, start);

  if (GST_CLOCK_TIME_IS_VALID (end)) {
    if (GST_BUFFER_DURATION_IS_VALID (buf))
      end += GST_BUFFER_DURATION (buf);

    end = gst_segment_to_stream_time (seg, GST_FORMAT_TIME, end);
  }

  GST_DEBUG ("Got buffer stream time %" GST_TIME_FORMAT " to %" GST_TIME_FORMAT,
      GST_TIME_ARGS (start), GST_TIME_ARGS (end));

  /* update the range of timestamps we've encountered */
  if (!GST_CLOCK_TIME_IS_VALID (first_ts) || start < first_ts)
    first_ts = start;
  if (!GST_CLOCK_TIME_IS_VALID (last_ts) || end > last_ts)
    last_ts = end;

  gst_sample_unref (sample);

  return GST_FLOW_OK;
}

static void
test_playbin_splitmuxsrc_pipeline (void)
{
  GstMessage *msg;
  GstElement *pipeline;
  GstElement *appsink;
  GstElement *fakesink2;
  gchar *uri;
  GstAppSinkCallbacks callbacks = { NULL };

  pipeline = gst_element_factory_make ("playbin", "playbin");
  if (!pipeline) {
    g_print ("Failed to create playbin pipeline\n");
    return;
  }

  appsink = gst_element_factory_make ("appsink", NULL);
  g_object_set (G_OBJECT (appsink), "sync", FALSE, NULL);

  g_object_set (G_OBJECT (pipeline), "video-sink", appsink, NULL);
  fakesink2 = gst_element_factory_make ("fakesink", NULL);
  g_object_set (G_OBJECT (pipeline), "audio-sink", fakesink2, NULL);

  uri = g_strdup_printf ("splitmux://%s", "matroska*.mkv");

  g_object_set (G_OBJECT (pipeline), "uri", uri, NULL);
  g_free (uri);

  callbacks.new_sample = receive_sample;
  gst_app_sink_set_callbacks (GST_APP_SINK (appsink), &callbacks, NULL, NULL);

  seek_pipeline (pipeline, 1.0, 0, -1);
  assert (first_ts == GST_CLOCK_TIME_NONE);

  GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS ((GstBin *) pipeline,
      GST_DEBUG_GRAPH_SHOW_ALL, "splitmuxsrc-eos-test-before-run");
  msg = run_pipeline (pipeline);
  GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS ((GstBin *) pipeline,
      GST_DEBUG_GRAPH_SHOW_ALL, "splitmuxsrc-eos-test-after-run");
  g_assert (GST_MESSAGE_TYPE (msg) != GST_MESSAGE_ERROR);
  g_assert (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_EOS);
  gst_message_unref (msg);
  gst_object_unref (pipeline);

  g_print ("Test of playbin pipeline succeeded\n");
}

GstPad *splitmuxsrc_audio = NULL;
GstPad *splitmuxsrc_video = NULL;

static void
stream_sync_pad_added (GstElement * stream_sync, GstPad * pad,
    GstElement * pipeline)
{
  gchar *name;

  name = gst_pad_get_name (pad);

  if (strcmp (name, "src_0") == 0) {
    GstElement *queue_0 = gst_bin_get_by_name (GST_BIN (pipeline), "queue_1");
    g_assert (queue_0 != NULL);

    GstPad *queue_0_sink_pad = gst_element_get_static_pad (queue_0, "sink");
    g_assert (gst_pad_link (pad, queue_0_sink_pad) == GST_PAD_LINK_OK);
    gst_object_unref (queue_0_sink_pad);
    gst_object_unref (queue_0);
  }

  if (strcmp (name, "src_1") == 0) {
    GstElement *queue_1 = gst_bin_get_by_name (GST_BIN (pipeline), "queue_2");
    g_assert (queue_1 != NULL);

    GstPad *queue_1_sink_pad = gst_element_get_static_pad (queue_1, "sink");
    g_assert (gst_pad_link (pad, queue_1_sink_pad) == GST_PAD_LINK_OK);
    gst_object_unref (queue_1_sink_pad);
    gst_object_unref (queue_1);
  }

  g_free (name);
}

static void
splitmuxsrc_no_more_pads (GstElement * splitmuxsrc, GstElement * pipeline)
{
  GstElement *stream_sync;
  GstElement *video_dec;
  GstPad *sync_audio, *sync_video;

  stream_sync = gst_bin_get_by_name (GST_BIN (pipeline), "stream_sync");
  video_dec = gst_bin_get_by_name (GST_BIN (pipeline), "video_decoder");
  g_assert (stream_sync != NULL);
  g_assert (video_dec != NULL);

  sync_video = gst_element_request_pad_simple (stream_sync, "sink_%u");
  g_assert (sync_video != NULL);

  GstPad *video_dec_sink = gst_element_get_static_pad (video_dec, "sink");
  GstPad *video_dec_src = gst_element_get_static_pad (video_dec, "src");
  g_assert (video_dec_sink != NULL);
  g_assert (video_dec_src != NULL);

  g_assert (gst_pad_link (splitmuxsrc_video,
          video_dec_sink) == GST_PAD_LINK_OK);
  g_assert (gst_pad_link (video_dec_src, sync_video) == GST_PAD_LINK_OK);

  sync_audio = gst_element_request_pad_simple (stream_sync, "sink_%u");
  g_assert (sync_audio != NULL);

  g_assert (gst_pad_link (splitmuxsrc_audio, sync_audio) == GST_PAD_LINK_OK);

  gst_object_unref (stream_sync);
  gst_object_unref (video_dec);
  gst_object_unref (sync_video);
  gst_object_unref (video_dec_sink);
  gst_object_unref (video_dec_src);
  gst_object_unref (sync_audio);
}

static void
splitmuxsrc_pad_added (GstElement * splitmuxsrc, GstPad * pad,
    GstElement * pipeline)
{
  gchar *pad_name = gst_pad_get_name (pad);

  if (strcmp (pad_name, "audio_0") == 0) {
    splitmuxsrc_audio = pad;
  }

  if (strcmp (pad_name, "video_0") == 0) {
    splitmuxsrc_video = pad;
  }

  g_free (pad_name);
}

static void
test_splitmuxsrc_pipeline (void)
{
  GstMessage *msg;
  GstElement *splitmuxsrc = NULL;
  GstElement *queue_1 = NULL;
  GstElement *queue_2 = NULL;
  GstElement *sink_1 = NULL;
  GstElement *sink_2 = NULL;
  GstElement *pipeline = NULL;
  GstElement *theora_dec = NULL;
  GstElement *stream_sync = NULL;

  theora_dec = gst_element_factory_make ("theoradec", "video_decoder");
  if (!theora_dec) {
    g_print ("Failed to create theora decoder");
    goto error;
  }

  stream_sync = gst_element_factory_make ("streamsynchronizer", "stream_sync");
  if (!stream_sync) {
    g_print ("Failed to create stream synchronizer");
    goto error;
  }

  splitmuxsrc = gst_element_factory_make ("splitmuxsrc", "splitmuxsrc");
  if (!splitmuxsrc) {
    g_print ("Failed to create splitmuxsrc\n");
    goto error;
  }
  g_object_set (G_OBJECT (splitmuxsrc), "location", "matroska*.mkv", NULL);

  queue_1 = gst_element_factory_make ("queue", "queue_1");
  if (!queue_1) {
    g_print ("Failed to create audio queue\n");
    goto error;
  }

  queue_2 = gst_element_factory_make ("queue", "queue_2");
  if (!queue_2) {
    g_print ("Failed to create video queue\n");
    goto error;
  }

  sink_1 = gst_element_factory_make ("fakesink", "sink_1");
  if (!sink_1) {
    g_print ("Failed to create audio sink\n");
    goto error;
  }
  g_object_set (G_OBJECT (sink_1), "sync", TRUE, NULL);

  sink_2 = gst_element_factory_make ("fakesink", "sink_2");
  if (!sink_2) {
    g_print ("Failed to create video sink\n");
    goto error;
  }
  g_object_set (G_OBJECT (sink_2), "sync", TRUE, NULL);

  pipeline = gst_pipeline_new ("splitmuxsrc-eos-test");

  gst_bin_add_many (GST_BIN (pipeline), splitmuxsrc, queue_1, queue_2, sink_1,
      sink_2, theora_dec, stream_sync, NULL);

  g_signal_connect (splitmuxsrc, "pad-added",
      G_CALLBACK (splitmuxsrc_pad_added), pipeline);
  g_signal_connect (splitmuxsrc, "no-more-pads",
      G_CALLBACK (splitmuxsrc_no_more_pads), pipeline);
  g_signal_connect (stream_sync, "pad-added",
      G_CALLBACK (stream_sync_pad_added), pipeline);

  if (!gst_element_link (queue_1, sink_1)) {
    g_print ("Could not link audio queue and audio sink\n");
    goto error;
  }

  if (!gst_element_link (queue_2, sink_2)) {
    g_print ("Could not link video queue and video sink\n");
    goto error;
  }

  seek_pipeline (pipeline, 1.0, 0, -1);
  assert (first_ts == GST_CLOCK_TIME_NONE);

  msg = run_pipeline (pipeline);
  g_assert (GST_MESSAGE_TYPE (msg) != GST_MESSAGE_ERROR);
  g_assert (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_EOS);
  gst_message_unref (msg);
  gst_object_unref (pipeline);

  g_print
      ("Test of simple splitmuxsrc, streamsynchronizer and queue pipeline succeeded\n");

  return;

error:
  if (theora_dec)
    gst_object_unref (theora_dec);
  if (stream_sync)
    gst_object_unref (stream_sync);
  if (pipeline)
    gst_object_unref (pipeline);
  if (splitmuxsrc)
    gst_object_unref (splitmuxsrc);
  if (queue_1)
    gst_object_unref (queue_1);
  if (queue_2)
    gst_object_unref (queue_2);
  if (sink_1)
    gst_object_unref (sink_1);
  if (sink_2)
    gst_object_unref (sink_2);
}

int
main (int argc, char *argv[])
{
  gst_init (NULL, NULL);

  test_playbin_splitmuxsrc_pipeline ();
  // test_splitmuxsrc_pipeline ();

  gst_deinit ();
}
